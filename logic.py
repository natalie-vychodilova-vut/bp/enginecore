from enum import Enum


class Operation(Enum):
    AND = 1
    OR = 2
    NOT = 3
    WHEN = 4


class Block:
    value: bool
    name: str


class Trigger(Block):
    def __init__(self, name: str, description: str):
        self.name = name
        self.description = description


class Operand(Block):
    operand: Operation
    one: Block
    two: Block

    def __init__(self, operand: Operation, one: Block, two: Block = None):
        self.one = one
        self.two = two
        self.operand = operand
        self.value = self.__set_value()
        self.name = self.__str__()

    def __set_value(self):
        if self.operand == Operation.WHEN:
            return self.one.value
        elif self.operand == Operation.NOT:
            return not self.one.value
        elif self.operand == Operation.AND:
            return self.one.value and self.two.value
        elif self.operand == Operation.OR:
            return self.one.value or self.two.value

    def __str__(self):
        if self.two is None:
            return f"{self.operand.name} {self.one.name} /[{self.one.value}]"
        else:
            return f"({self.one.name} {self.operand.name} {self.two.name}) /[{self.one.value}]"


if __name__ == "__main__":
    a = Trigger("A", "xxx")
    b = Trigger("B", "xxx")
    c = Trigger("C", "xxx")

    a.value = True
    b.value = False
    c.value = True

    # Rovnice: (a AND b) OR (NOT c)
    result = Operand(Operation.OR, Operand(Operation.AND, a, b), Operand(Operation.NOT, c))
    print(result.value)

    # Rovnice: ((a OR b) OR ((NOT c) AND a))
    result = Operand(Operation.OR, Operand(Operation.OR, a, b), Operand(Operation.AND, Operand(Operation.NOT, c), a))
    print(result.value)
