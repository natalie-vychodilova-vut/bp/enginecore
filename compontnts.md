### ActionTemplate:
#### Database:
- id
- name
- description
- function
- params
- created

#### ON create:
- name
- description
- function
- params

#### ON list view:
- id
- name
- description


#### ON detail view:
- id
- name
- description
- params


### Action:
#### Database:
- id
- params
- created
- action_template

#### ON create:
- ActionTemplate id
- params

#### ON list view:
- id
- name action template
- description action template
- action template id
- params

#### ON detail view:
- id
- name action template
- description action template
- action template id
- params


### TriggerTemplate:
#### Database:
- id
- name
- description
- function
- params
- created

#### ON create:
- name
- description
- function
- params

#### ON list view:
- id
- name
- description


#### ON detail view:
- id
- name
- description
- params


### Trigger:
#### Database:
- id
- params
- created

#### ON create:
- TriggerTemplate id
- params

#### ON list view:
- id
- name Trigger template
- description Trigger template
- Trigger template id
- params

#### ON detail view:
- id
- name Trigger template
- description Trigger template
- Trigger template id
- params

### Rule
#### Database:
- id
- name
- description
- created
- is_active
- actions
- block
- last_triggered (datetime)

#### ON create:
- name
- description
- actionIDs []
- incomingBlock {}

#### ON list view
- id
- name
- is_active
- last_triggered


### RuleTemplate
#### Database:
- id
- name
- description
- created
- action_templates
- trigger_template

#### ON create:
- name
- description
- action_templates_ids []
- trigger_template_id {}

#### ON list view
- id
- name