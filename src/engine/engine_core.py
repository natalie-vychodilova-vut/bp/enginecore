from business_rules import run_all, export_rule_data
from business_rules.actions import BaseActions, rule_action
from business_rules.fields import FIELD_NUMERIC
from business_rules.variables import BaseVariables, numeric_rule_variable, string_rule_variable, select_rule_variable


import datetime

from django.core.management.base import BaseCommand
from api.components.rule import Rule, RuleEngineSerializer


from rules import rules


class Rule:
    def __init__(self, name, age, group):
        self.name = name
        self.age = age
        self.group = group

    def __str__(self):
        return f"{self.name}: {self.age} ({self.group})"


class RuleVariables(BaseVariables):
    def __init__(self, person):
        self.person = person

    @string_rule_variable
    def person_name(self):
        return self.person.name

    @numeric_rule_variable(label='Days until expiration')
    def person_age(self):
        return self.person.age

    @string_rule_variable()
    def person_group(self):
        return datetime.datetime.now().strftime("%B")


class RuleActions(BaseActions):
    def __init__(self, person):
        self.person = person

    @rule_action(params={"person_age": FIELD_NUMERIC})
    def put_on_sale(self, person_age):
        self.person.age = person_age + 1

    @rule_action(params={"group_number": FIELD_NUMERIC})
    def order_more(self, group_number):
        print("action executed" + group_number)

    @rule_action(params={"number_phone": FIELD_NUMERIC})
    def print_name(self, number_phone):
        print("action executed " + number_phone)



if __name__ == "__main__":
    export_rule_data(RuleVariables, RuleActions)

    rules = [
        {"conditions": {"all": [
            {"name": "person_name",
             "operator": "equal_to",
             "value": "naty",
             }
        ]},
            "actions": [
                {"name": "print_name",
                 "params": {"number_phone": '785555555'},
                 },
            ],
        }]

    product1 = Rule('naty', 20, 2)
    product2 = Rule('jirka', 22, 1)

    products = [product1, product2]
    for product in products:
        print(product)
        run_all(rule_list=rules,
                defined_variables=RuleVariables(product),
                defined_actions=RuleActions(product),
                stop_on_first_trigger=True
                )


