rules = [
    {
        'conditions': {
            'feed_id': {'equal_to': '728'},
            'status': {'equal_to': 'out'}
        },
        'actions': [{'name': 'send_notification', 'params': {'message': 'Zone 46 has been breached.'}}]
    },
    {
        'conditions': {
            'feed_id': {'equal_to': '729'},
            'status': {'equal_to': 'out'}
        },
        'actions': [{'name': 'send_notification', 'params': {'message': 'Zone 46 has been breached.'}}]
    },
]