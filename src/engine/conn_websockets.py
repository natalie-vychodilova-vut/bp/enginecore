import websocket
import json
import requests


DEST_URI = "ws://demo.sewio.net:8080"
X_API_KEY = "17254faec6a60f58458308763"
REST_URI = 'https://demo.sewio.net/sensmapserver/api'

DATA_LIST = []


class Zone:
    def __init__(self, id, name):
        self.id = id
        self.name = name


def request(buildingId, plan):
    url = REST_URI + "/buildings/" + buildingId + "/plans/" + plan + "/zones"
    headers = {'X-ApiKey': 0}
    headers['X-ApiKey'] = X_API_KEY
    output = requests.get(url, headers=headers)
    return output.json()


def on_message(ws, message):
    data = json.loads(message)
    body = data['body']
    for obj in DATA_LIST:
        if obj.id == body['zone_id']:
            print(data)


def on_error(ws, error):
    print(error)


def on_close(ws, close_status_code, close_msg):
    print("### closed ###")


def on_open(ws):
    print("### opened ###")
    messages = [
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/feeds/"},
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/restrictions/"},
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/zones/"}
    ]

    print("Sending message...")

    print(json.dumps(messages[2]))
    ws.send(json.dumps(messages[2]))



if __name__ == "__main__":
    object = request('137', 'coils')
    for item in object:
        DATA_LIST.append(Zone(item['id'], item['name']))

    ws = websocket.WebSocketApp(DEST_URI, on_open=on_open, on_message=on_message, on_error=on_error, on_close=on_close)
    ws.run_forever()

