from djongo import models
from django import forms
from rest_framework import serializers, generics


class ActionTemplate(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    data = models.JSONField(default=dict, blank=False)


class ActionTemplateForm(forms.ModelForm):
    model = ActionTemplate
    fields = '__all__'


class ActionTemplateSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    data = serializers.JSONField()

    class Meta:
        model = ActionTemplate
        fields = ['id', 'name', 'description', 'created', 'data']

    def get_id(self, obj):
        return str(obj._id)

    def get_data(self, obj):
        return dict(obj.data)


class ActionTemplateCreateAPIView(generics.ListCreateAPIView):
    queryset = ActionTemplate.objects.all()
    serializer_class = ActionTemplateSerializer
