from djongo import models
from django import forms
from rest_framework import serializers, generics

import re

from api.components.action_template import ActionTemplate, ActionTemplateForm
from api.functions import prettier_id, get_foreign_model, create_name


from rest_framework.exceptions import ValidationError


class Action(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=200, unique=True)
    data = models.JSONField(default=dict, blank=False)
    action_template = models.EmbeddedField(model_container=ActionTemplate, model_form_class=ActionTemplateForm, null=True)

    objects = models.DjongoManager()


class ActionForm(forms.ModelForm):
    model = Action
    fields = '__all__'


class ActionSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    data = serializers.JSONField()
    action_template = serializers.SerializerMethodField(read_only=True)

    action_template_id = serializers.CharField(write_only=True)

    class Meta:
        model = Action
        fields = ('id', 'name', 'data', 'action_template_id', 'action_template')
        read_only_fields = ['name']

    def get_id(self, obj):
        return str(obj._id)

    def get_data(self, obj):
        return dict(obj.data)

    def get_action_template(self, obj):
        out = prettier_id(obj.action_template)
        return out


class ActionCreateAPIView(generics.ListCreateAPIView):
    queryset = Action.objects.all()
    serializer_class = ActionSerializer

    def perform_create(self, serializer):
        action_template_id = serializer.validated_data.pop('action_template_id')
        action_template = get_foreign_model(ActionTemplate, action_template_id, return_dict=True)
        name = create_name(action_template['name'], serializer.validated_data.get('data')['params'])

        queryset = Action.objects.filter(name=name)
        if queryset.exists():
            raise ValidationError('This Action already exists!')

        serializer.is_valid(raise_exception=True)
        serializer.save(action_template=action_template, name=name)



