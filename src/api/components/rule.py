import bson
import json

from djongo import models
from rest_framework import serializers, generics
from dataclasses import asdict

from api.functions import prettier_id, get_foreign_model, create_rule_name, is_data, \
    get_data, create_name, crate_name_for_trigger,validate_trigger_data
from .action import Action, ActionForm
from .action_template import ActionTemplate
from .trigger_template import TriggerTemplate
from .trigger import Trigger, TriggerForm

from rest_framework.exceptions import ValidationError


class Rule(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=False)
    last_triggerd = models.DateTimeField(null=True)

    trigger = models.EmbeddedField(model_container=Trigger, model_form_class=TriggerForm, null=True)
    actions = models.ArrayField(model_container=Action, model_form_class=ActionForm, null=True)

    objects = models.DjongoManager()


class RuleSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    is_active = serializers.SerializerMethodField(read_only=True)
    last_triggerd = serializers.SerializerMethodField(read_only=True)

    trigger = serializers.SerializerMethodField(read_only=True)
    actions = serializers.SerializerMethodField(read_only=True)

    actions_block = serializers.ListField(write_only=True)
    trigger_block = serializers.JSONField(write_only=True)

    class Meta:
        model = Rule
        fields = ['id', 'name', 'created', 'is_active', 'last_triggerd', 'trigger',
                  'actions', 'actions_block', 'trigger_block']
        read_only_fields = ['name']

    def get_id(self, obj):
        return str(obj._id)

    def get_is_active(self, obj):
        return obj.is_active

    def get_last_triggerd(self, obj):
        return obj.last_triggerd

    def get_actions(self, obj):
        out = []
        for action in obj.actions:
            out.append(prettier_id(action))
        return out

    def get_trigger(self, obj):
        out = prettier_id(obj.trigger)
        return out


class RuleEngineSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField()

    class Meta:
        model = Rule
        fields = ['data']

    def get_data(self, obj):
        return {"conditions": dict(get_data(obj.trigger)),
                "actions": [dict(get_data(action)) for action in obj.actions]}


class RuleSimpleViewSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Rule
        fields = ['id', 'name', 'is_active', 'last_triggerd']

    def get_id(self, obj):
        return str(obj._id)


class RuleUpdateSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Rule
        fields = ['id', 'is_active']

    def get_id(self, obj):
        return str(obj._id)


class RuleCreateAPIView(generics.ListCreateAPIView):
    queryset = Rule.objects.all()
    serializer_class = RuleSerializer

    def perform_create(self, serializer):
        actions_block = serializer.validated_data.pop('actions_block')
        trigger_block = serializer.validated_data.pop('trigger_block')

        trigger_template_in = get_foreign_model(TriggerTemplate, trigger_block['id'], return_dict=True)
        trigger_name = crate_name_for_trigger(trigger_template_in['name'], trigger_block['data'])

        queryset = Trigger.objects.filter(name=trigger_name)
        if queryset.exists():
            trigger = Trigger.objects.get(name=trigger_name)
        else:
            trigger = Trigger.objects.create(trigger_template=trigger_template_in, data=validate_trigger_data(trigger_block['data']), name=trigger_name)

        trigger_out = trigger.__dict__
        trigger_out.pop('_state')

        actions = []
        actions_names = []
        for in_action_template in actions_block:
            action_template = get_foreign_model(ActionTemplate, in_action_template['id'], return_dict=True)
            is_data(in_action_template['data']['params'])
            actions_name = create_name(name=action_template['name'], data=in_action_template['data']['params'])
            actions_names.append(actions_name)

            queryset = Action.objects.filter(name=actions_name)
            if queryset.exists():
                action = Action.objects.get(name=actions_name)
            else:
                action = Action.objects.create(action_template=action_template, data=in_action_template['data'], name=actions_name)

            action_out = action.__dict__
            action_out.pop('_state')
            actions.append(action_out)

        name = create_rule_name(trigger_name=trigger_name, action_names=actions_names)
        print(name)

        queryset = Rule.objects.filter(name=name)
        if queryset.exists():
            raise ValidationError('This rule already exists!')

        serializer.is_valid(raise_exception=True)
        serializer.save(actions=actions, trigger=trigger_out, name=name)


class RuleEngineAPIView(generics.ListAPIView):
    queryset = Rule.objects.all()
    serializer_class = RuleEngineSerializer


class RuleUpdateView(generics.UpdateAPIView):
    queryset = Rule.objects.all()
    serializer_class = RuleUpdateSerializer

    lookup_field = "_id"


class RuleAPIView(generics.ListAPIView):
    queryset = Rule.objects.all()
    serializer_class = RuleSimpleViewSerializer
