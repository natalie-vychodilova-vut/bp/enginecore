from djongo import models
from django import forms
from rest_framework import serializers
from rest_framework import generics


import bson
from .action import Action

from .block import Block
from .trigger_template import TriggerTemplate, TriggerTemplateForm
from api.functions import prettier_id, get_foreign_model, create_name, crate_name_for_trigger, get_data_from_block
from rest_framework.exceptions import ValidationError

class Trigger(Block, models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=200, unique=True)
    data = models.JSONField(default=dict, blank=False)
    trigger_template = models.EmbeddedField(model_container=TriggerTemplate, model_form_class=TriggerTemplateForm,
                                            null=True)

    objects = models.DjongoManager()


class TriggerForm(forms.ModelForm):
    model = Trigger
    fields = '__all__'


class TriggerSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    data = serializers.JSONField()
    trigger_template = serializers.SerializerMethodField(read_only=True)

    trigger_template_id = serializers.CharField(write_only=True)

    class Meta:
        model = Trigger
        fields = ['id', 'name', 'data', 'trigger_template_id', 'trigger_template']
        read_only_fields = ['name']

    def get_id(self, obj):
        return str(obj._id)

    def get_data(self, obj):
        return dict(obj.data)

    def get_trigger_template(self, obj):
        out = prettier_id(obj.trigger_template)
        return out


class TriggerCreateAPIView(generics.ListCreateAPIView):
    queryset = Trigger.objects.all()
    serializer_class = TriggerSerializer

    def perform_create(self, serializer):
        trigger_template_id = serializer.validated_data.pop('trigger_template_id')
        trigger_template = get_foreign_model(TriggerTemplate, trigger_template_id, return_dict=True)
        name = crate_name_for_trigger(name=trigger_template['name'], block=serializer.validated_data.get('data'))

        queryset = Trigger.objects.filter(name=name)
        if queryset.exists():
            raise ValidationError('This Trigger already exists!')

        serializer.is_valid(raise_exception=True)
        serializer.save(trigger_template=trigger_template, name=name)
