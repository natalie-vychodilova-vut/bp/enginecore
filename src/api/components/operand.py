from .block import Block


class Operand(Block):
    operand_type: str

    def __init__(self, operand_type, left, right):
        self.left = left
        self.right = right
        self.operand_type = operand_type
        self.value = self.define_value()

    def __str__(self):
        return f"({self.left} {self.operand_type} {self.right})"

    def define_value(self):
        if self.operand_type == "":
            return self.left.value
        elif self.operand_type == "NOT":
            return not self.left.value
        elif self.operand_type == "AND":
            return self.left.value and self.right.value
        elif self.operand_type == "OR":
            return self.left.value or self.right.value
