from djongo import models
from rest_framework import serializers, generics

from api.functions import prettier_id, get_foreign_model
from .action_template import ActionTemplate, ActionTemplateForm
from .trigger_template import TriggerTemplate, TriggerTemplateForm


class RuleTemplate(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    action_templates = models.ArrayField(model_container=ActionTemplate, model_form_class=ActionTemplateForm, null=True)
    trigger_template = models.EmbeddedField(model_container=TriggerTemplate, model_form_class=TriggerTemplateForm,
                                             null=True)

    objects = models.DjongoManager()


class RuleTemplateSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    action_templates = serializers.SerializerMethodField(read_only=True)
    trigger_template = serializers.SerializerMethodField(read_only=True)

    action_templates_ids = serializers.ListField(write_only=True)
    trigger_template_id = serializers.CharField(write_only=True)

    class Meta:
        model = RuleTemplate
        fields = ['id', 'name', 'description', 'created', 'action_templates_ids', 'trigger_template_id', 'action_templates', 'trigger_template']

    def get_id(self, obj):
        return str(obj._id)

    def get_action_templates(self, obj):
        out = []
        for action_template in obj.action_templates:
            out.append(prettier_id(action_template))
        return out

    def get_trigger_template(self, obj):
        out = prettier_id(obj.trigger_template)
        return out


class RuleTemplateNameSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = RuleTemplate
        fields = ['id', 'name', 'description']

    def get_id(self, obj):
        return str(obj._id)


class RuleTemplateCreateAPIView(generics.ListCreateAPIView):
    queryset = RuleTemplate.objects.all()
    serializer_class = RuleTemplateSerializer

    def perform_create(self, serializer):
        action_templates_ids = serializer.validated_data.pop('action_templates_ids')
        trigger_template_id = serializer.validated_data.pop('trigger_template_id')

        trigger_template = get_foreign_model(TriggerTemplate, trigger_template_id, return_dict=True)

        action_templates = []
        for action_template_id in action_templates_ids:
            action_template = get_foreign_model(ActionTemplate, action_template_id, return_dict=True)
            action_templates.append(action_template)

        serializer.is_valid(raise_exception=True)
        serializer.save(action_templates=action_templates, trigger_template=trigger_template)


class RuleTemplateAPIView(generics.ListAPIView):
    queryset = RuleTemplate.objects.all()
    serializer_class = RuleTemplateNameSerializer



