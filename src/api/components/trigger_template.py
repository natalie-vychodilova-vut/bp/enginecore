from djongo import models
from django import forms
from rest_framework import serializers
from rest_framework import generics


class TriggerTemplate(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    data = models.JSONField(default=dict, blank=True)


class TriggerTemplateForm(forms.ModelForm):
    model = TriggerTemplate
    fields = '__all__'


class TriggerTemplateSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField(read_only=True)
    data = serializers.JSONField()

    class Meta:
        model = TriggerTemplate
        fields = ['id', 'name', 'description', 'created', 'data']

    def get_id(self, obj):
        return str(obj._id)

    def get_data(self, obj):
        return dict(obj.data)


class TriggerTemplateCreateAPIView(generics.ListCreateAPIView):
    queryset = TriggerTemplate.objects.all()
    serializer_class = TriggerTemplateSerializer

