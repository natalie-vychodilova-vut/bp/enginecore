from django.core.management.base import BaseCommand
from api.models import Triggers
import json


class Command(BaseCommand):
    """Django command to wait for database."""

    def handle(self, *args, **options):
        data_path = "data/trigger_templates.json"

        f = open(data_path)
        triggers_data = json.load(f)
        for tr in triggers_data:
            trigger = Triggers(**tr)
            print(trigger.__dict__)
            trigger.save()

        f.close()
