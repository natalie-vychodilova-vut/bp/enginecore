from django.core.management.base import BaseCommand
from api.models import Triggers, Block, Operand, Rules
from api.serializers import OperandSerializer, TriggerSerializer
import json
from bson import ObjectId


class Command(BaseCommand):
    """Django command to wait for database."""

    def handle(self, *args, **options):



        trigger1 = Triggers.objects.get(_id=ObjectId('6429a87aa87d259d95dc8d56'))
        trigger2 = Triggers.objects.get(_id=ObjectId('6429a87aa87d259d95dc8d57'))


        trigger1.value = True
        trigger2.value = False


        trser1 = TriggerSerializer(trigger1, many=False)

        print(trser1.data)

        operand_and = Operand(left=trigger1, right=trigger2, operand_type="AND")

        osr = OperandSerializer(operand_and)

        print(osr.data)

        print(operand_and.value)
        #operand_ser = OperandSerializer(operand_and)

        #operand_and2 = Operand(left=trigger3, right=operand_and, operand_type="AND")
#
        #operand_not = Operand(left=operand_and2, operand_type="NOT")
#
#
#
        #rule = Rules(name="R", description="RUle", created='2023-03-30T08:10:36.141+00:00', is_active=False, block=operand_not)
#
        #print(rule.to_json())

