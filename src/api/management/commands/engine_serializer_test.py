import datetime

from django.core.management.base import BaseCommand
from api.components.rule import Rule, RuleEngineSerializer
from api.components.action_template import ActionTemplate, ActionTemplateSerializer

from api.components.action import Action, ActionSerializer

import json
from bson import ObjectId

from api.components.trigger import Trigger


class Command(BaseCommand):
    """Django command to wait for database."""

    def handle(self, *args, **options):
        rules = []

        rules_objects = Rule.objects.filter(is_active__in=[True])
        for rule in rules_objects:
            ruleSer = RuleEngineSerializer(rule, many=False)
            rules.append(ruleSer.data['data'])

        block = {'zone_id': '8', 'status': 'out', 'duration': 3}
        string = "tag was in zone {zone_id} longer than {time}"
        formatted_string = string.format(time=block['duration'], **block)


        data = {'all': [{'name': 'zone_id', 'operator': 'equal_to', 'value': '45'},
                        {'name': 'status', 'operator': 'equal_to', 'value': 'in'},
                        {'name': 'duration', 'operator': 'equal_to', 'value': '45'}
                        ]}

        for item in data['all']:
            print(item)
            if item['name'] == 'duration':
                item['value'] = int(item['value'])

        print(data)













