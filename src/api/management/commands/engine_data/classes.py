from business_rules.actions import BaseActions, rule_action
from business_rules.fields import FIELD_NUMERIC, FIELD_TEXT
from business_rules.variables import BaseVariables, string_rule_variable, numeric_rule_variable
import datetime


class Zone:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class Data:
    def __init__(self, feed_id, zone_id, status, at, duration=None):
        self.feed_id = feed_id
        self.zone_id = zone_id
        self.status = status
        self.at = at
        self.duration = duration


class RuleVariables(BaseVariables):
    def __init__(self, rule):
        self.rule = rule

    @string_rule_variable(label='Id of the zone')
    def zone_id(self):
        return self.rule.zone_id

    @string_rule_variable(label='Zone status')
    def status(self):
        return self.rule.status

    @string_rule_variable(label='Id of the feed')
    def feed_id(self):
        return self.rule.feed_id

    @string_rule_variable(label='Timestamp of the event')
    def at(self):
        return self.rule.at

    @numeric_rule_variable(label='Tags time in zone')
    def duration(self):
        return self.rule.duration


class RuleActions(BaseActions):
    def __init__(self, rule):
        self.rule = rule

    @rule_action(params={"message": FIELD_TEXT})
    def send_to_app(self, message):
        print("rule with action: " + message)

    @rule_action(params={"message": FIELD_TEXT})
    def print_message(self, message):
        print("rule with action: " + message)
