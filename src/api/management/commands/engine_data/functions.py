import requests
from business_rules import export_rule_data, run_all
import json
from bson import ObjectId

from api.components.rule import RuleEngineSerializer, Rule

from .classes import RuleVariables, RuleActions, Zone, Data

REST_URI = 'https://demo.sewio.net/sensmapserver/api'
X_API_KEY = "17254faec6a60f58458308763"

active_rules = []


def request(rest_url, key, buildingId, plan):
    url = rest_url + "/buildings/" + buildingId + "/plans/" + plan + "/zones"
    headers = {'X-ApiKey': key}
    output = requests.get(url, headers=headers)
    return output.json()


def create_data_dict(data):
    duration = data.get('duration', None)
    if duration is None:
        duration = 0
    return {'feed_id': data['feed_id'], 'zone_id': data['zone_id'], 'status': data['status'], 'at': data['at'],
            'duration': duration}


def engine_core(data):
    data_query = Data(**create_data_dict(data))

    rules_objects = Rule.objects.filter(is_active__in=[True])
    for rule in rules_objects:
        ruleSer = RuleEngineSerializer(rule, many=False)
        active_rules.append(ruleSer.data['data'])

    export_rule_data(RuleVariables, RuleActions)
    run_all(rule_list=active_rules,
            defined_variables=RuleVariables(data_query),
            defined_actions=RuleActions(data_query),
            stop_on_first_trigger=True
            )
