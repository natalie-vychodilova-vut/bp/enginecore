from django.core.management import BaseCommand
import websocket
import json

from .engine_data.classes import Zone
from .engine_data.functions import request, engine_core


DEST_URI = "ws://demo.sewio.net:8080"
X_API_KEY = "17254faec6a60f58458308763"


def on_message(ws, message):
    data = json.loads(message)
    engine_core(data['body'])


def on_error(ws, error):
    print(error)


def on_close(ws, close_status_code, close_msg):
    print("### closed ###")


def on_open(ws):
    print("### opened ###")
    messages = [
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/feeds/"},
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/restrictions/"},
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/zones/"}
    ]
    print("Sending message...")
    print(json.dumps(messages[2]))
    ws.send(json.dumps(messages[2]))


class Command(BaseCommand):
    def handle(self, *args, **options):
        ws = websocket.WebSocketApp(DEST_URI,
                                    on_open=on_open,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close)
        ws.run_forever()
