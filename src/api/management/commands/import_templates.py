from django.core.management.base import BaseCommand
from api.models import Templates, Triggers, Actions
import json


def get_data(filename, object_type):
    data_path = "data/" + filename
    f = open(data_path)
    data = json.load(f)
    data_array = []
    for d in data:
        data_model = object_type(**d)
        data_array.append(data_model.name)
    f.close()
    return data_array


class Command(BaseCommand):
    """Django command to wait for database."""

    def handle(self, *args, **options):
        triggers = get_data("trigger_templates.json", Triggers)
        actions = get_data("action_templates.json", Actions)

        for i in range(len(triggers)):
            trigger = Triggers.objects.get(name=triggers[i])
            action = Actions.objects.get(name=actions[0])

            template = Templates()
            template.name = action.name + " when " + trigger.name
            template.description = "Rule " + template.name
            template.triggers = trigger.__dict__
            template.actions = action.__dict__
            template.save()
