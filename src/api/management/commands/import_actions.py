from django.core.management.base import BaseCommand
from api.models import Actions
import json


class Command(BaseCommand):
    """Django command to wait for database."""

    def handle(self, *args, **options):
        data_path = "data/action_templates.json"

        f = open(data_path)
        actions_data = json.load(f)
        for ac in actions_data:
            action = Actions(**ac)
            print(action.__dict__)
            action.save()
        f.close()
