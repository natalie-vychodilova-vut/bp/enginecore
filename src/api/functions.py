import bson
from rest_framework.exceptions import NotFound
from collections import OrderedDict
from rest_framework.exceptions import ValidationError
import re

def prettier_id(dictionary: dict) -> dict:
    new_dict = {}
    for key, value in dictionary.items():
        if key == '_id':
            new_key = "id"
        else:
            new_key = key
        if isinstance(value, dict):
            new_dict[new_key] = prettier_id(value)
        elif isinstance(value, bson.ObjectId):
            new_dict[new_key] = str(value)
        else:
            new_dict[new_key] = value

    return new_dict


def get_data(dictionary: dict) -> dict:
    new_dict = {}
    for key, value in dictionary.items():
        if key == 'data':
            new_dict = value
    for key, value in new_dict.items():
        if isinstance(new_dict, OrderedDict):
            return dict(zip(new_dict.keys(), [convert_to_dict(v) for v in new_dict.values()]))
    return dict(new_dict)


def convert_to_dict(obj):
    if isinstance(obj, OrderedDict):
        return dict(zip(obj.keys(), [convert_to_dict(v) for v in obj.values()]))
    elif isinstance(obj, list):
        return [convert_to_dict(i) for i in obj]
    elif isinstance(obj, tuple):
        return tuple([convert_to_dict(i) for i in obj])
    elif isinstance(obj, dict):
        return {k: convert_to_dict(v) for k, v in obj.items()}
    else:
        return obj


def replace_objectId(obj):
    if isinstance(obj, bson.ObjectId):
        return str(obj)
    return obj


def get_foreign_model(model, str_id: str, return_dict=False):
    try:
        foreign_model = model.objects.get(_id=bson.ObjectId(str_id))
        if return_dict:
            foreign_model_dict = foreign_model.__dict__
            foreign_model_dict.pop('_state')
            return foreign_model_dict
        else:
            return foreign_model

    except bson.errors.InvalidId:
        raise NotFound(detail='ObjectID wrong format', code=None)

    except model.DoesNotExist:
        raise NotFound(detail=f"Object with ObjectID {str_id} does not exists.")


def get_valid_model(model, str_id: str, return_dict=False):
    try:
        foreign_model = model.objects.get(_id=bson.ObjectId(str_id))
        if return_dict:
            foreign_model_dict = foreign_model.__dict__
            foreign_model_dict.pop('_state')
            return foreign_model_dict
        else:
            return foreign_model

    except bson.errors.InvalidId:
        raise NotFound(detail='ObjectID wrong format', code=None)

    except model.DoesNotExist:
        raise NotFound(detail=f"Object with ObjectID {str_id} does not exists.")


def create_name(name, data):
    pattern = r"\{(.*?)\}"
    matches = re.findall(pattern, name)

    for match in matches:
        if match in data:
            name = name.replace("{" + match + "}", '<' + data[match] + '>')
    return name


def get_data_from_block(data_block):
    data = {}
    for item in data_block['all']:
        if item['value'] is None:
            raise ValidationError('Data is empty!')
        name = item['name']
        value = item['value']
        data[name] = value
    return data


def crate_name_for_trigger(name, block):
    data = get_data_from_block(block)
    name = create_name(name, data)
    return name


def action_block(action_names):
    output = ''
    for i in action_names:
        output += i + ' and '
    output = output[:-5]
    return output


def create_rule_name(trigger_name, action_names):
    name = "When {trigger_name} then {action_names}."
    name = name.replace("{trigger_name}", trigger_name)
    name = name.replace("{action_names}", action_block(action_names))
    return name


def is_data(data):
    if all(val is None for val in data.values()):
        raise ValidationError('Data is empty!')


def validate_trigger_data(data_block):
    for item in data_block['all']:
        if item['name'] == 'duration':
            item['value'] = int(item['value'])
    return data_block

