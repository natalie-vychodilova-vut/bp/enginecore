from django.urls import path, register_converter

from . import views

from api.components.trigger_template import TriggerTemplateCreateAPIView
from api.components.rule import RuleCreateAPIView, RuleUpdateView, RuleEngineAPIView, RuleAPIView
from .components.action import ActionCreateAPIView
from .components.action_template import ActionTemplateCreateAPIView
from .components.rule_template import RuleTemplateCreateAPIView, RuleTemplateAPIView
from .components.trigger import TriggerCreateAPIView

from api.converters.objectid import ObjectIdConverter


register_converter(ObjectIdConverter, 'ObjectId')


urlpatterns = [
    path('test', views.test),

    #TRIGGERS:
    path('templates/triggers', TriggerTemplateCreateAPIView.as_view()),
    path('triggers', TriggerCreateAPIView.as_view()),

    #ACTIONS:
    path('templates/actions', ActionTemplateCreateAPIView.as_view()),
    path('actions', ActionCreateAPIView.as_view()),

    #RULES:
    path('templates/rules', RuleTemplateCreateAPIView.as_view()),

    path('rules', RuleAPIView.as_view()),
    path('rules_all', RuleCreateAPIView.as_view()),
    path('rules/<ObjectId:_id>', RuleUpdateView.as_view()),

    #ENGINE:
    path('engine', RuleEngineAPIView.as_view()),
]