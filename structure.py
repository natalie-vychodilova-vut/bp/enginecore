import json
from enum import Enum


class Operation(Enum):
    AND = "and"
    OR = "or"
    NOT = "not"


class Trigger:
    def __init__(self, trigger_id, name, parameters):
        self.trigger_id = trigger_id
        self.name = name
        self.parameters = parameters

    def get_json(self):
        return {"Trigger_id": self.trigger_id,
                "name": self.name,
                "parameters": self.parameters}


class Operand:
    def __init__(self, operand, first, second=None):
        self.operand = operand
        self.first = first
        self.second = second

    def get_json(self):
        if self.operand == Operation.NOT:
            return {'operand': self.operand.value, 'trigger': self.first.get_json()}
        else:
            return {"operand": self.operand.value,
                    "left": self.first.get_json(),
                    "right": self.second.get_json()}


if __name__ == "__main__":

    a = Trigger("1", "type_1", {"param_1": "value_1"})
    b = Trigger("2", "type_2", {"param_2": "value_2"})
    c = Trigger("3", "type_3", {"param_3": "value_3"})

# ((a AND b) OR ((NOT c) OR a))
    op1 = Operand(Operation.AND, a, b)

    op2 = Operand(Operation.NOT, c)
    op3 = Operand(Operation.OR, op2, a)
    op4 = Operand(Operation.OR, op1, op3)
    json_data = op4.get_json()
    print(json.dumps(json_data))
